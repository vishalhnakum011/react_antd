import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Login from "../src/pages/Login";
import Register from "../src/pages/Register";
import 'antd/dist/antd.css';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
// import rootReducer from './reducers'
import todoApp from './reducers'



// const store = createStore(rootReducer)
let store = createStore(todoApp)

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route exect path="/register" component={Register} />
          <Route exect path="/" component={Login} />
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
