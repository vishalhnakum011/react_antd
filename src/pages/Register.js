import React, { Component } from 'react'
import { Form, Row, Col, Icon, Input, Button, Checkbox } from 'antd';
import axios from "axios"
import { Link } from 'react-router-dom';

class Register extends Component {

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    };
    componentDidMount() {
        // axios.post('http://192.168.43.224:8000/api/v1/login', {
        //     email: 'vishalhnakum2@gmail.com',
        //     password: 'vishal@123'
        // })
        //     .then(function (response) {
        //         console.log(response);
        //     })
        //     .catch(function (error) {
        //         console.log(error);
        //     });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (
            <div >
                <Row justify="center" type="flex" align="middle" style={{ minHeight: "100vh" }}>
                    <Col span={6}>
                        <h1>Register</h1>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Item>
                                {getFieldDecorator('email', {
                                    rules: [{ required: true, message: 'Please input your email!' }],
                                })(
                                    <Input
                                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        placeholder="Email"
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Please input your Password!' }],
                                })(
                                    <Input
                                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        type="password"
                                        placeholder="Password"
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Submit
                            </Button>
                            </Form.Item>
                            Or <Link to="/">Login!</Link>

                        </Form>
                    </Col>
                </Row>
            </div >

        )
    }
}
// const RegisterForm = Form.create({ name: 'register' })(Register);
export default Form.create({ name: 'register' })(Register);
