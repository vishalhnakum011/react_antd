import React, { Component } from 'react'
import { Form, Row, Col, Icon, Input, Button, Checkbox } from 'antd';
import axios from "axios"
import { connect } from 'react-redux'

import { Link } from 'react-router-dom';
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            action: null,
        }
    }

    handleSubmit = e => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
            }
        });
    };
    componentDidMount() {
        console.log("did maount", this.props)
        // axios.post('http://192.168.43.224:8000/api/v1/login', {
        //     email: 'vishalhnakum2@gmail.com',
        //     password: 'vishal@123'
        // })
        //     .then(function (response) {
        //         console.log(response);
        //     })
        //     .catch(function (error) {
        //         console.log(error);
        //     });
    }

    render() {
        console.log("DFH")
        const { getFieldDecorator } = this.props.form;
        return (
            <div >
                <Row justify="center" type="flex" align="middle" style={{ minHeight: "100vh" }}>
                    <Col span={6}>
                        <h1>Login</h1>
                        <Form onSubmit={this.handleSubmit}>
                            <Form.Item>
                                {getFieldDecorator('email', {
                                    rules: [{ required: true, message: 'Please input your email!' }],
                                })(
                                    <Input
                                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        placeholder="Email"
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('password', {
                                    rules: [{ required: true, message: 'Please input your Password!' }],
                                })(
                                    <Input
                                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                                        type="password"
                                        placeholder="Password"
                                    />,
                                )}
                            </Form.Item>
                            <Form.Item>
                                {getFieldDecorator('remember', {
                                    valuePropName: 'checked',
                                    initialValue: true,
                                })(<Checkbox>Remember me</Checkbox>)}
                                <a className="login-form-forgot" href="">
                                    Forgot password
                                </a>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" className="login-form-button">
                                    Log in
                            </Button>
                            </Form.Item>
                            Or <Link to="/register">register now!</Link>

                        </Form>
                    </Col>
                </Row>
            </div >

        )
    }
}
const LoginForm = Form.create({ name: 'login' })(Login);
const mapStateToProps = (state) => {
    return {
        active: state.visibilityFilter
    }
}
export default connect(mapStateToProps)(LoginForm);
